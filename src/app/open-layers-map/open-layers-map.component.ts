import { AfterViewInit, Component, ElementRef, ViewChild } from '@angular/core';
import { ToolbarComponentType } from '../models/toolbar-componenttype';
import { Lookup } from '../models/lookup';
import { RestDataService } from '../services/rest-service';
import { ModalComponent } from '../share/modal/modal.component';
import { QueryResultComponent } from '../tools/query-result/query-result.component';
import { TreeNode } from 'primeng/primeng';
import { Config } from '../config/config';
import { MapMeaurement } from '../models/map-measurement';

declare var ol: any;

@Component({
  moduleId: module.id,
  selector: 'app-openlayers-map',
  templateUrl: 'open-layers-map.component.html'
})
export class OpenLayersMapComponent implements AfterViewInit {
  @ViewChild('mapElement') mapElement: ElementRef;
  @ViewChild('queryResult') QueryResultComponent: QueryResultComponent;

  private queryPopupModal: ModalComponent;
    public map: any;
    public isOpenLayerDiv = true;
    private defaultLat = 24.6849;
    private defaultLong = -22.3285;
    private defailtZoom = 7;
    private zoomInteraction;

    private drawingMode = false;
    private querydrawing: any;
    private source: any;
    private layers: any[] = [];
    private selectedLayerId: number = null;

    private selectedService: TreeNode;
    private servicesList: TreeNode[] = [];
    private serviceName = '';

    private treeLayers: TreeNode[] = [];
    private selectedTreeLayers: TreeNode[] = [];

    private requestedLayers: TreeNode[] = [];

    private currentLayer: any;

    private sketch: any;
    private helpTooltipElement: any;
    private helpTooltip: any;
    private measureTooltipElement: any;
    private measureTooltip: any;
    private continuePolygonMsg = 'Click to continue drawing the polygon';
    private continueLineMsg = 'Click to continue drawing the line';
    private draw: any;
    private listener: any;
    private drawingVector: any;
    private drawingSource: any;
    private currentDrawingType: string;

    reloadLayers(selectedNode: TreeNode) {
        this.serviceName = selectedNode.label;
        this.treeLayers = [];
        this.selectedTreeLayers = [];
        this.layers = [];
        this.map.removeLayer(this.currentLayer);
        this.currentLayer = null;
        this.selectedLayerId = null;
        this.requestedLayers = [];
        this.loadMapLayers();
    }

    loadMapServices() {
        this.restDataService.getMapServices(Config.serverName).subscribe((resp: any) => {
            let count = 1;
            resp.forEach((x: any) => {
                if (x.type === 'MapServer') {
                    const newLayer: TreeNode = {
                        label: x.name,
                        data: count,
                        expanded: true
                    };
                this.servicesList.push(newLayer);
                count = count + 1;
            }
            });

            if (this.servicesList.length > 0) {
                this.serviceName = this.servicesList[0].label;
                this.selectedService = this.servicesList[0];
                this.loadMapLayers();
            }
        });
    }

    loadMapLayers(ignoreVisibilityCheck: boolean = false) {
        this.restDataService.getMapLayers(Config.serverName, this.serviceName).subscribe((resp: any) => {
            const layerIDs: number[] = [];
            const rootLayers = resp.filter((x: any) => x.parentLayerId === -1);
            rootLayers.forEach((x: any) => {
                const newLayer: TreeNode = {
                    label: x.name,
                    data: x.id,
                    expanded: true,
                    children: this.loadChildrenLayer(x.subLayerIds, resp, ignoreVisibilityCheck)
                };
                this.treeLayers.push(newLayer);
                if (ignoreVisibilityCheck || x.defaultVisibility) {
                    if (!newLayer.children || newLayer.children.length === 0) {
                        this.requestedLayers.push(newLayer);
                        this.addToLayer(x);
                    }
                    this.selectedTreeLayers.push(newLayer);
                }
            });
            this.requestedLayers.forEach((x: TreeNode) => {
                layerIDs.push(x.data);
            });
            this.createLayersOnMaps(layerIDs.join());
        });
    }

    addToLayer(layer: any) {
        if (this.layers.length === 0) {
            this.selectedLayerId = layer.id;
        }

        this.layers.push({
            value: layer.id,
            label: layer.name
        });
    }

    loadChildrenLayer(subLayerIds, source, ignoreVisibilityCheck) {
        const result: any[] = [];

        if (subLayerIds) {
            subLayerIds.forEach((x: number) => {
                const filterLayer = source.find((y: any) => y.id === x);
                if (filterLayer) {
                    const newLayer: TreeNode = {
                        label: filterLayer.name,
                        data: filterLayer.id,
                        expanded: true,
                        children: this.loadChildrenLayer(filterLayer.subLayerIds, source, ignoreVisibilityCheck)
                    };
                    result.push(newLayer);

                    if (ignoreVisibilityCheck || filterLayer.defaultVisibility) {
                        if (!newLayer.children || newLayer.children.length === 0) {
                            this.requestedLayers.push(newLayer);
                            this.addToLayer(filterLayer);
                        }
                        this.selectedTreeLayers.push(newLayer);
                    }
                }
            });
        }

        return result;
    }

    createLayersOnMaps(layerIDs: string) {
         this.currentLayer = new ol.layer.Tile({
          source: new ol.source.TileArcGISRest({
            url: 'http://' + Config.serverName + '/arcgis/rest/services/' + this.serviceName + '/MapServer',
            params: {'LAYERS': 'show:' + layerIDs},
            crossOrigin: 'anonymous'
          })
        });
        if (layerIDs !== '') {
            this.map.addLayer(this.currentLayer);
            this.map.render();
        }
    }

    constructor(private restDataService: RestDataService) {
        this.loadMapServices();

        const that = this;

        const osm_layer: any = new ol.layer.Tile({
            source: new ol.source.OSM()
        });



        // note that the target cannot be set here!
        this.map = new ol.Map({
            controls: ol.control.defaults().extend([
                new ol.control.FullScreen()
            ]),
            interactions : ol.interaction.defaults({shiftDragZoom: false}),
            layers: [osm_layer],
            view: new ol.View({
            center: ol.proj.transform([this.defaultLat, this.defaultLong], 'EPSG:4326', 'EPSG:3857'),
            zoom: this.defailtZoom
            })
        });

        // add zoom slider
        const zoomslider = new ol.control.ZoomSlider();
        this.map.addControl(zoomslider);

        // this.map.on( 'dblclick', function (e) {
        //     if (that.drawingMode) {
        //         return false;
        //     }
        // });

        this.map.on('pointermove', function(evt) {
            if (evt.dragging) {
                return;
            }

            let helpMsg = 'Click to start drawing';

            if (that.sketch) {
                const geom = (that.sketch.getGeometry());
                if (geom instanceof ol.geom.Polygon) {
                    helpMsg = that.continuePolygonMsg;
                } else if (geom instanceof ol.geom.LineString) {
                    helpMsg = that.continueLineMsg;
                }
            }
            if (that.helpTooltipElement && that.helpTooltip) {
                that.helpTooltipElement.innerHTML = helpMsg;
                that.helpTooltip.setPosition(evt.coordinate);

                that.helpTooltipElement.classList.remove('hidden');
            }
        });

        this.map.getViewport().addEventListener('mouseout', function() {
            if (that.helpTooltipElement) {
                that.helpTooltipElement.classList.add('hidden');
            }
        });

        this.source = new ol.source.Vector({wrapX: false});
        this.source.on('addfeature', function(evt){
            const latlngs: any[] = [];
            const feature = evt.feature;
            const coords = feature.getGeometry().getCoordinates();
            coords[0].forEach(x => {
                const latlng =  ol.proj.transform(x, 'EPSG:3857', 'EPSG:4326');
                latlngs.push(latlng);
            });

            that.restDataService.querying(Config.serverName, that.serviceName, that.selectedLayerId,
                latlngs[2][0], latlngs[2][1], latlngs[0][0], latlngs[0][1], false).subscribe((resp: any) => {
                that.queryPopupModal.show();

                const resultData: any[] = [];
                const resultFields: any[] = [];

                if (resp.features) {
                    resp.features.forEach((item: any) => {
                        resultData.push(item.attributes);
                    });
                }
                if (resp.fields) {
                    resp.fields.forEach((field: any) => {
                        resultFields.push(field);
                    });
                }
                that.QueryResultComponent.refresh(resultFields, resultData);
            });
        });
    }

    addLayerOnMap(node: TreeNode) {
        this.selectedTreeLayers.push(node);

        this.map.removeLayer(this.currentLayer);

        if (!node.children || node.children.length === 0) {
            this.requestedLayers.push(node);
        }
        this.addToLayerSource(node);

        this.map.addLayer(this.currentLayer);
        this.map.render();
    }

    addToLayerSource(node: TreeNode) {
        if (!node.children || node.children.length === 0) {
            const source = this.currentLayer.getSource();
            const params = source.getParams();
            if (params.LAYERS === '') {
                params.LAYERS = 'show:' + node.data;
            } else {
                params.LAYERS = params.LAYERS + ',' + node.data;
            }
            source.updateParams(params);
            source.changed();
        } else {
            node.children.forEach((x: TreeNode) => this.addToLayerSource(x));
        }
    }

    removeLayerFromList(node: TreeNode) {
        const index = this.selectedTreeLayers.indexOf(node);
        if (index >= 0) {
            this.selectedTreeLayers.splice(index, 1);
        }

        if (node.children) {
            node.children.forEach((x: TreeNode) => {
               this.removeLayerFromList(x);
            });
        }
    }

    removeLayerFromSource(node: TreeNode) {
        if (!node.children || node.children.length === 0) {
            const index = this.requestedLayers.indexOf(node);
            if (index >= 0) {
                this.requestedLayers.splice(index, 1);
            }
            let showLayers = '';
            let count = 0;
            this.requestedLayers.forEach((x: TreeNode) => {
                if (count === 0) {
                    showLayers = 'show:' + x.data;
                } else {
                    showLayers = showLayers + ',' + x.data;
                }
                count = count + 1;
            });

            const source = this.currentLayer.getSource();
            const params = source.getParams();
            params.LAYERS = showLayers;
            source.updateParams(params);
            source.changed();
        } else {
            node.children.forEach((x: TreeNode) => this.removeLayerFromSource(x));
        }
    }

    removeLayerOnMap(node: TreeNode) {
        this.removeLayerFromList(node);
        this.map.removeLayer(this.currentLayer);

        this.removeLayerFromSource(node);


        if (this.requestedLayers.length > 0) {
            this.map.addLayer(this.currentLayer);
        }
        this.map.render();
    }

    queryPopupModalLoaded(modal: ModalComponent) {
        this.queryPopupModal = modal; // Here you get a reference to the modal so you can control it programmatically
    }

    onOpenHandler(event: any) {
    }

    onCloseConfirmation(event: any) {}


    ngAfterViewInit() {
        this.map.setTarget(this.mapElement.nativeElement.id);
    }

    setDefaultCenter() {
        this.map.getView().setCenter(ol.proj.transform([this.defaultLat, this.defaultLong], 'EPSG:4326', 'EPSG:3857'));
        this.map.getView().setZoom(this.defailtZoom);
    }

    setZoomIn() {
        if (this.zoomInteraction) {
            this.map.removeInteraction(this.zoomInteraction);
        }
        this.zoomInteraction = new ol.interaction.DragZoom({
            duration: 10,
            out: false,
            condition: false // no extra key required
        });
        this.map.addInteraction(this.zoomInteraction);
    }

    setZoomOut() {
        if (this.zoomInteraction) {
            this.map.removeInteraction(this.zoomInteraction);
        }
        this.zoomInteraction = new ol.interaction.DragZoom({
            duration: 10,
            out: true,
            condition: false // no extra key required
        });
        this.map.addInteraction(this.zoomInteraction);
    }

    setToPan() {
        if (this.zoomInteraction) {
            this.map.removeInteraction(this.zoomInteraction);
            this.zoomInteraction = null;
        }
    }

    saveToPng() {
        this.map.once('postcompose', function(event) {
          const canvas = event.context.canvas;
          if (navigator.msSaveBlob) {
            navigator.msSaveBlob(canvas.msToBlob(), 'map.png');
          } else {
            canvas.toBlob(function(blob) {
              const url = window.URL.createObjectURL(blob);
              const a = document.createElement('a');
              a.href = url;
              a.download = 'map.png';
              a.click();
              window.URL.revokeObjectURL(url);
            });
          }
        });
        this.map.renderSync();
    }

    print() {
        window.print();
    }

    querying() {
        if (this.querydrawing) {
            this.map.removeInteraction(this.querydrawing);
            this.querydrawing = null;
        }
        this.querydrawing = new ol.interaction.Draw({
            source: this.source,
            type: /** @type {ol.geom.GeometryType} */ ('Circle'),
            geometryFunction: ol.interaction.Draw.createBox()
          });
        const that = this;
        this.querydrawing.on('drawend', function (event) {
            that.map.removeInteraction(that.querydrawing);
            that.querydrawing = null;
            that.map.getInteractions().forEach(function(interaction) {
                if (interaction instanceof ol.interaction.DragPan) {
                    interaction.setActive(true);
                }
            }, this);
        });
        this.map.addInteraction(this.querydrawing);
        this.drawingMode = true;
        this.map.getInteractions().forEach(function(interaction) {
            if (interaction instanceof ol.interaction.DragPan) {
                interaction.setActive(false);
            }
        }, this);
    }

    fireEvent(componentType: ToolbarComponentType) {
        switch (componentType) {
            case ToolbarComponentType.FullContent:
                this.setDefaultCenter();
                break;
            case ToolbarComponentType.ZoomIn:
                this.setZoomIn();
                break;
            case ToolbarComponentType.ZoomOut:
                this.setZoomOut();
                break;
            case ToolbarComponentType.Pan:
                this.setToPan();
                break;
            case ToolbarComponentType.SaveToPNG:
                this.saveToPng();
                break;
             case ToolbarComponentType.Print:
                this.print();
                break;
            case ToolbarComponentType.Query:
                this.querying();
                break;
            case ToolbarComponentType.Layer:
                if (this.isOpenLayerDiv) {
                    this.isOpenLayerDiv = false;
                } else {
                    this.isOpenLayerDiv = true;
                }
                break;
            case ToolbarComponentType.Measurement:
                break;
            case ToolbarComponentType.ClearMeasurement:
                this.removeAlldrawingFeatures();
                break;
            default:
                break;
        }
    }

    updateTargetLayer(targetLayer: number) {
        this.selectedLayerId = targetLayer;
    }

    opacityOnchange(value: number) {
        this.currentLayer.setOpacity(value / 100);
    }

    fireMeaurementEvent(value: MapMeaurement) {
        if (value.isDrawing) {
            if (this.draw) {
                this.map.removeInteraction(this.draw);
            }
            this.currentDrawingType = value.type;
            this.addInteraction(value.type);
        } else {
            this.map.removeInteraction(this.draw);
            this.map.removeLayer(this.drawingVector);
            const staticTooltips = document.getElementsByClassName('tooltip-static');
            while (staticTooltips.length > 0) {
                const staticTooltip = staticTooltips[0];
                if (staticTooltip) {
                    staticTooltip.parentNode.removeChild(staticTooltip);
                }
            }
            this.sketch = null;
            this.helpTooltipElement = null;
            this.helpTooltip = null;
            this.measureTooltipElement = null;
            this.measureTooltip = null;
            this.draw = null;
            this.drawingVector = null;
            this.drawingSource = null;
            this.currentDrawingType = null;
        }
    }

    formatLength(line) {
        const wgs84Sphere = new ol.Sphere(6378137);

        let length = 0;
        const coordinates = line.getCoordinates();
        const sourceProj = this.map.getView().getProjection();
        const ii = coordinates.length - 1;
        for (let i = 0; i < ii; ++i) {
            const c1 = ol.proj.transform(coordinates[i], sourceProj, 'EPSG:4326');
            const c2 = ol.proj.transform(coordinates[i + 1], sourceProj, 'EPSG:4326');
            length += wgs84Sphere.haversineDistance(c1, c2);
          }

        let output;
        if (length > 100) {
          output = (Math.round(length / 1000 * 100) / 100) +
              ' ' + 'km';
        } else {
          output = (Math.round(length * 100) / 100) +
              ' ' + 'm';
        }
        return output;
    }

    formatArea(polygon) {
        const wgs84Sphere = new ol.Sphere(6378137);
        let area = 0;
        const sourceProj = this.map.getView().getProjection();
        const geom = /** @type {ol.geom.Polygon} */(polygon.clone().transform(
              sourceProj, 'EPSG:4326'));
        const coordinates = geom.getLinearRing(0).getCoordinates();
          area = Math.abs(wgs84Sphere.geodesicArea(coordinates));

        let output;
        if (area > 10000) {
          output = (Math.round(area / 1000000 * 100) / 100) +
              ' ' + 'km<sup>2</sup>';
        } else {
          output = (Math.round(area * 100) / 100) +
              ' ' + 'm<sup>2</sup>';
        }
        return output;
      };

    addInteraction(typeSelect) {
        if (!this.drawingSource) {
            this.drawingSource = new ol.source.Vector({wrapX: false});
        }
        if (!this.drawingVector) {
            this.drawingVector = new ol.layer.Vector({
                source: this.drawingSource
            });
            this.map.addLayer(this.drawingVector);
        }

        const type = (typeSelect === 'area' ? 'Polygon' : 'LineString');
        this.draw = new ol.interaction.Draw({
          source: this.drawingSource,
          type: (type)
        });

        this.createMeasureTooltip();
        this.createHelpTooltip();

        this.map.addInteraction(this.draw);

        const that = this;

        that.draw.on('drawstart', function(evt) {
              // set sketch
              that.sketch = evt.feature;

              let tooltipCoord = evt.coordinate;

              that.listener = that.sketch.getGeometry().on('change', function(e: any) {
                const geom = e.target;
                let output;
                if (geom instanceof ol.geom.Polygon) {
                  output = that.formatArea(geom);
                  tooltipCoord = geom.getInteriorPoint().getCoordinates();
                } else if (geom instanceof ol.geom.LineString) {
                  output = that.formatLength(geom);
                  tooltipCoord = geom.getLastCoordinate();
                }
                that.measureTooltipElement.innerHTML = output;
                that.measureTooltip.setPosition(tooltipCoord);
              });
            }, that);


        that.draw.on('drawend', function() {
              that.measureTooltipElement.className = 'tooltip tooltip-static';
              that.measureTooltip.setOffset([0, -7]);
              // unset sketch
              that.sketch = null;
              // unset tooltip so that a new one can be created
              that.measureTooltipElement = null;
              that.createMeasureTooltip();
              ol.Observable.unByKey(that.listener);
            }, that);

        this.map.render();

    }

    createHelpTooltip() {
        if (this.helpTooltipElement) {
          this.helpTooltipElement.parentNode.removeChild(this.helpTooltipElement);
        }
        this.helpTooltipElement = document.createElement('div');
        this.helpTooltipElement.className = 'tooltip hidden';
        this.helpTooltip = new ol.Overlay({
          element: this.helpTooltipElement,
          offset: [15, 0],
          positioning: 'center-left'
        });
        this.map.addOverlay(this.helpTooltip);
      }

    createMeasureTooltip() {
        if (this.measureTooltipElement) {
          this.measureTooltipElement.parentNode.removeChild(this.measureTooltipElement);
        }
        this.measureTooltipElement = document.createElement('div');
        this.measureTooltipElement.className = 'tooltip tooltip-measure';
        this.measureTooltip = new ol.Overlay({
          element: this.measureTooltipElement,
          offset: [0, -15],
          positioning: 'bottom-center'
        });
        this.map.addOverlay(this.measureTooltip);
      }

      removeAlldrawingFeatures() {
        this.map.removeInteraction(this.draw);
        this.map.removeLayer(this.drawingVector);
        const staticTooltips = document.getElementsByClassName('tooltip-static');
        while (staticTooltips.length > 0) {
            const staticTooltip = staticTooltips[0];
            if (staticTooltip) {
                staticTooltip.parentNode.removeChild(staticTooltip);
            }
        }
        this.sketch = null;
        this.helpTooltipElement = null;
        this.helpTooltip = null;
        this.measureTooltipElement = null;
        this.measureTooltip = null;
        this.draw = null;
        this.drawingVector = null;
        this.drawingSource = null;
        this.addInteraction(this.currentDrawingType);
      }
}
