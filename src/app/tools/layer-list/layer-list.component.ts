import { Component, Output, EventEmitter, Input } from '@angular/core';
import { TreeNode } from 'primeng/primeng';

@Component({
  moduleId: module.id,
  selector: 'app-layer-list',
  templateUrl: 'layer-list.component.html'
})
export class LayerListComponent {
    @Input() layers: TreeNode[] = [];
    @Input() selectedLayers: TreeNode[] = [];

    @Output() addLayer = new EventEmitter<TreeNode>();
    @Output() removeLayer = new EventEmitter<TreeNode>();

    constructor() {

    }

     nodeSelect(event) {
        this.addLayer.next(event.node);
    }

    nodeUnselect (event) {
        this.removeLayer.next(event.node);
    }
}
