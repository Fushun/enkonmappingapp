import { Component, Output, EventEmitter, Input } from '@angular/core';
import { TreeNode } from 'primeng/primeng';

import { RestDataService } from '../../services/rest-service';
import { MapLegend } from '../../models/map-legend';

import { Config } from '../../config/config';

@Component({
  moduleId: module.id,
  selector: 'app-map-legend',
  templateUrl: 'legend.component.html'
})
export class MapLegendComponent {
    _serviceName: string;
    @Input()
    set serviceName(data: string) {
      this._serviceName = data;
      this.legends = [];
      if (data && data !== '') {
        this.loadLegend();
      }
    }
    get serviceName() {
      return this._serviceName;
    }


    private legends: MapLegend[] = [];

    constructor(private restDataService: RestDataService) {
    }

    loadLegend() {
        const that = this;
        this.restDataService.getMapLegend(Config.serverName, this.serviceName).subscribe((resp: any) => {
            if (resp) {
                resp.forEach((x: any) => {
                    if (x.legend) {
                        x.legend.forEach((l: any) => {
                            const newLegend: MapLegend = {
                                label: x.layerName,
                                url: l.url,
                                contentType: l.contentType,
                                height: l.height,
                                width: l.width,
                                imageData: l.imageData
                            };
                            that.legends.push(newLegend);
                        });
                    }
                });
            }
        });
    }
}
