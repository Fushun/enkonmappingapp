import { Component, Output, EventEmitter, Input, ViewChild } from '@angular/core';

@Component({
  moduleId: module.id,
  selector: 'app-query-result',
  templateUrl: 'query-result.component.html'
})
export class QueryResultComponent {
    private rowData: any[];
    private columnDefs: any[];
    private isOpen = false;
    constructor() {
        this.rowData = [];
        this.columnDefs = [];
    }

    refresh(fields: any[], data: any[]) {
        this.isOpen = true;
        this.columnDefs = [];
        fields.forEach((x: any) => {
            let wid = x.alias.length * 8;
            if (wid < 140) {
                wid = 140;
            } else if (wid > 200) {
                wid = 200;
            }
            const newColumn: any = {
                header: x.alias,
                field: x.name.split('.').join('').split('(').join('').split(')').join(''),
                width: wid + 'px'
            };
            this.columnDefs.push(newColumn);
        });

        const newDataList = [];
        data.forEach((d: any) => {
            const ob = new Object;
            fields.forEach((f: any) => {
                let value = d[f.name];
                if (value) {
                    value = value.toString();
                } else {
                    value = '';
                }
                ob[f.name.split('.').join('').split('(').join('').split(')').join('').replace(/ /g, '')] =
                    value.replace('null', '').replace('Null', '').replace('NULL', '');
            });
            newDataList.push(ob);
        });

        this.rowData = newDataList;
    }
}
