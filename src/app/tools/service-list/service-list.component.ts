import { Component, Output, EventEmitter, Input } from '@angular/core';
import { TreeNode } from 'primeng/primeng';

@Component({
  moduleId: module.id,
  selector: 'app-service-list',
  templateUrl: 'service-list.component.html'
})
export class ServiceListComponent {
    @Input() mapServicesList: TreeNode[] = [];
    @Input() selectedMapService: TreeNode;

    @Output() reloadLayers = new EventEmitter<TreeNode>();

    constructor() {

    }

     nodeSelect(event) {
        this.reloadLayers.next(event.node);
    }
}
