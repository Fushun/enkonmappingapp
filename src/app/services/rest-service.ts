import { Injectable } from '@angular/core';
import { Headers, RequestOptions, Http, Response } from '@angular/http';
import { Observable, Subject } from 'rxjs/Rx';


/**
 * RestDataService
 *
 * @export
 * @class RestDataService
 */
@Injectable()
export class RestDataService {
  httpOptions: RequestOptions;
  private proxyUrl = '';
  constructor(private http: Http) {
    // JSON headers
    const headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.set('Content-Type', 'application/json');
    headers.append('Access-Control-Allow-Origin', '*');
    headers.append('Access-Control-Allow-Methods', 'GET,POST,PUT,DELETE,OPTIONS');
    headers.append('Access-Control-Allow-Headers', 'Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With');
    this.httpOptions = new RequestOptions({ headers: headers });
  }

  /**
   * Querying items
   */
  public querying<TModel>(serveName: string, serviceName: string, layerId: number,
    nelng: number, nelat: number, swlng: number, swlat: number, idOnly: boolean, objectIds: string = ''): Observable<Array<TModel>> {

      const subject = new Subject<Array<TModel>>();

        let url = this.getServiceUrl(serveName, serviceName, false, '');
        url += '/' + layerId + '/query?text=&geometry=' + String(nelng) + ',' + String(nelat) + ',' + String(swlng) + ',' + String(swlat);
        url += '&objectIds=' + objectIds + '&returnIdsOnly=' + String(idOnly ? 'true' : 'false');
        url += '&geometryType=esriGeometryEnvelope&spatialRel=esriSpatialRelIntersects&inSR=4326';
        url += '&outSR=4326&where=&returnGeometry=true&outFields=*&f=json';

      this.http.get(url, this.httpOptions)
        .subscribe((data: Response) => {
          const obj = data.json();

          subject.next(obj as Array<TModel>);
        }, (error: Response) =>  {
          subject.next([]);
          // this.showError(error);
        });

      return subject;
  }

  public getMapServices<TModel>(serveName: string): Observable<Array<TModel>> {

      const subject = new Subject<Array<TModel>>();
        let url = this.getServerUrl(serveName, false, '');
        url += '?f=json';

      this.http.get(url, this.httpOptions)
        .subscribe((data: Response) => {
          const obj = data.json();

          subject.next(obj.services as Array<TModel>);
        }, (error: Response) =>  {
          subject.next([]);
          // this.showError(error);
        });

      return subject;
  }

  public getMapLayers<TModel>(serveName: string, serviceName: string): Observable<Array<TModel>> {

      const subject = new Subject<Array<TModel>>();
        let url = this.getServiceUrl(serveName, serviceName, false, '');
        url += '?f=json';

      this.http.get(url, this.httpOptions)
        .subscribe((data: Response) => {
          const obj = data.json();

          subject.next(obj.layers as Array<TModel>);
        }, (error: Response) =>  {
          subject.next([]);
          // this.showError(error);
        });

      return subject;
  }

  getServiceUrl (serverName: string, serviceName: string, includeProxy: boolean, proxyUrl: string) {
        return this.getServerUrl(serverName, false, '') + serviceName + '/MapServer';
    };

  getServerUrl (serverName: string, includeProxy: boolean, proxyUrl: string) {
        let res = 'http://' + serverName + '/ArcGIS/rest/services/';
        if (includeProxy && proxyUrl && proxyUrl !== '') {
            res = proxyUrl + '?' + res;
        };
        return res;
    }


    public getMapLegend<TModel>(serveName: string, serviceName: string): Observable<Array<TModel>> {

      const subject = new Subject<Array<TModel>>();
        let url = this.getServiceUrl(serveName, serviceName, false, '');
        url += '/legend?f=json';

      this.http.get(url, this.httpOptions)
        .subscribe((data: Response) => {
          const obj = data.json();

          subject.next(obj.layers as Array<TModel>);
        }, (error: Response) =>  {
          subject.next([]);
          // this.showError(error);
        });

      return subject;
  }
}
