import { Component, Input, Output, EventEmitter, OnInit, ViewChild, ElementRef } from '@angular/core';


/**
 * The possible reasons a modal has been closed.
 */
export enum ModalAction { POSITIVE, CANCEL }


/**
 * Models the result of closing a modal dialog.
 */
export interface ModalResult {
  action: ModalAction;
  value?: any;
}


/**
 * ModalComponent
 *
 * Shows a bootstrap modal dialog.
 * Set the body of the dialog by adding content to the modal tag: <modal>content here</modal>.
 *
 * @export
 * @class ModalComponent
 * @implements {OnInit}
 */
@Component({
  moduleId: module.id,
  // tslint:disable-next-line
  selector: 'modal',
  templateUrl: 'modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit {

  @Input() title: string;
  @Input() cancelLabel = 'Cancel';
  @Input() positiveLabel = 'OK';
  @Input() btnOkDisabled = false;
  @Input() hideCancelButton = false;
  @Input() dialogWidth: number; // = null;
  @Input() dialogMaxHeight = 200; // = null;
  @Input() isDraggable = false;

  /**
   * Fires an event when the modal is closed. The argument indicated how it was closed.
   * @type {EventEmitter<ModalResult>}
   */
  @Output() closed = new EventEmitter<ModalResult>();

  /**
   * Fires an event when the modal is ready with a pointer to the modal.
   * @type {EventEmitter<Modal>}
   */
  @Output() loaded = new EventEmitter<ModalComponent>();
  @Output() opened = new EventEmitter<ModalComponent>();
  @ViewChild('modalDialog')  modalDialog: ElementRef;
  @ViewChild('modal')  modal: ElementRef;
  showModal = false;
  private returnValue: any;

  constructor() {}

  ngOnInit() {
    if (this.dialogWidth !== null) {
      this.modalDialog.nativeElement.style.width = this.dialogWidth + 'px';
    }

    this.modalDialog.nativeElement.style.maxHeight = this.dialogMaxHeight + 'px';

    this.loaded.next(this);
    this.returnValue = this.returnValue || { value: 0 };
  }

  /**
   * Shows the modal. There is no method for hiding. This is done using actions of the modal itself.
   */
  show() {
    const that = this;
    this.showModal = true;
    setTimeout(function() {
      that.opened.emit(this);
    }, 500);
  }

  positiveAction() {
    this.showModal = false;
    this.closed.next({
      action: ModalAction.POSITIVE,
      value: this.returnValue
    });
    return false;
  }

  cancelAction() {
    this.showModal = false;
    this.closed.next({
      action: ModalAction.CANCEL
    });
    return false;
  }
}
