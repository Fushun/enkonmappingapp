export interface MapLegend {
  label?: string;
  url?: string;
  imageData?: string;
  contentType?: string;
  height?: number;
  width?: number;
}
