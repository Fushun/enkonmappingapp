export enum ToolbarComponentType {
    Undefined,
    ZoomIn,
    ZoomOut,
    FullContent,
    Measurement,
    Print,
    Query,
    Geocoding,
    MapType,
    Layer,
    Pan,
    SaveToPNG,
    Locator,
    Seperator,
    ClearMeasurement
}
