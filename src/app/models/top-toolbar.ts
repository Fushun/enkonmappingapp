import { ToolbarComponentType } from './toolbar-componenttype';

export interface TopToolBar {
  Id?: number;
  ImgSrc?: string;
  Title?: string;
  Component?: ToolbarComponentType;
}
