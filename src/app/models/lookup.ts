export interface Lookup {
  value?: number;
  label?: string;
}
