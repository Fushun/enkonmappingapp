import { MapLegend } from './map-legend';

export interface MapLayer {
  layerId?: number;
  layerName?: string;
  layerType?: string;
  legend?: MapLegend[];
}
