import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MapComponent } from './map/map.component';
import { AdminComponent } from './admin/admin.component';

export const routes: Routes = [
  { path: '',
    pathMatch: 'full',
    component: MapComponent },
  { path: 'admin',
    pathMatch: 'full',
    component: AdminComponent }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }

export const routedComponents = [MapComponent, AdminComponent];
