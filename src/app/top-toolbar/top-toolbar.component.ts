import { Component, Output, EventEmitter, Input } from '@angular/core';
import { TopToolBar } from '../models/top-toolbar';
import { ToolbarComponentType } from '../models/toolbar-componenttype';
import { Lookup } from '../models/lookup';
import { MapMeaurement } from '../models/map-measurement';

@Component({
  moduleId: module.id,
  selector: 'app-top-toolbar',
  templateUrl: 'top-toolbar.component.html',
    styleUrls: ['./top-toolbar.component.css']
})
export class TopToolbarComponent {
    @Output() fireEvent = new EventEmitter<ToolbarComponentType>();
    @Output() fireMeaurementEvent = new EventEmitter<MapMeaurement>();
    @Output() changeTargetLayer = new EventEmitter<number>();
    @Output() opacityOnchange = new EventEmitter<number>();
    _value: number = null;
    @Input()
    set value(data: number) {
      this._value = data;
      this.selectedValue = data;
    }

    get value() {
      return this._value;
    }
    @Input() items: Lookup[] = [];
    private topToolbarMessage = '';
    private tools: TopToolBar[] = [];
    private selectedValue: number = null;

    private defaultOpacity = 100;
    private toolbarComponentType: any;

    private currentAction: ToolbarComponentType;

    private drawingTypeItems: Lookup[] = [];
    private drawingType: number;
    private selectedDrawingType: string;

    private isDrawingMode = false;

    constructor() {
        this.drawingTypeItems.push({
            label: 'length',
            value: 1
        }, {
            label: 'area',
            value: 2
        });
      this.toolbarComponentType = ToolbarComponentType;
      this.drawingType = this.drawingTypeItems[0].value;
      this.selectedDrawingType = this.drawingTypeItems[0].label;
    }


    toolbarClick(componentType: ToolbarComponentType) {
       this.currentAction = componentType;
       this.updateMessage( this.currentAction);
       if (componentType === ToolbarComponentType.Measurement) {
           this.selectedDrawingType = this.drawingTypeItems[0].label;
           if (this.isDrawingMode) {
               this.isDrawingMode = false;
           } else {
               this.isDrawingMode = true;
           }
           const mapMeaurement: MapMeaurement = {
                type: this.selectedDrawingType,
                isDrawing: this.isDrawingMode
           };
           this.fireMeaurementEvent.next(mapMeaurement);
       } else {
           this.fireEvent.next(componentType);
       }
    }

    updateMessage(componentType: ToolbarComponentType) {
      const that = this;
      switch (componentType) {
            case ToolbarComponentType.FullContent:
                this.topToolbarMessage = 'reset map back to default center and zoom level';
                setTimeout(function () {
                    that.clearMessage();
                }, 3000);
                break;
            case ToolbarComponentType.ZoomIn:
                this.topToolbarMessage = 'hold "Shift" button and draw rectangle on map to zoom in';
                break;
            case ToolbarComponentType.ZoomOut:
                this.topToolbarMessage = 'hold "Shift" button and draw rectangle on map to zoom out';
                break;
            case ToolbarComponentType.Pan:
                this.topToolbarMessage = 'swith to Pan mode';
                setTimeout(function () {
                    that.clearMessage();
                }, 3000);
                break;
            case ToolbarComponentType.SaveToPNG:
                this.topToolbarMessage = 'save map to png';
                setTimeout(function () {
                    that.clearMessage();
                }, 3000);
                break;
             case ToolbarComponentType.Print:
                this.topToolbarMessage = 'print map';
                setTimeout(function () {
                    that.clearMessage();
                }, 3000);
                break;
            case ToolbarComponentType.Query:
                this.topToolbarMessage = 'draw rectangle on map and all interested polygons will be returned within target layer';
                break;
            case ToolbarComponentType.Layer:
                this.topToolbarMessage = 'show/hide map theme';
                setTimeout(function () {
                    that.clearMessage();
                }, 3000);
                break;
            case ToolbarComponentType.Measurement:
                if (!this.isDrawingMode) {
                    this.topToolbarMessage = 'start drawing selected shape on map for measurement';
                } else {
                    this.topToolbarMessage = 'exit measreument and all drawing shapes will be removed';
                    setTimeout(function () {
                        that.clearMessage();
                    }, 3000);
                }
                break;
            default:
                break;
        }
    }

    clearMessage() {
      this.topToolbarMessage = '';
    }

    onChange(e) {
      this.value = e.value;
      this.changeTargetLayer.next(this.value);
    }

    opacityOnChange(e) {
      this.opacityOnchange.next(e.value);
    }

    drawingTypeOnChange(e) {
        this.selectedDrawingType = this.drawingTypeItems.find((x: Lookup) => x.value === e.value).label;
        const mapMeaurement: MapMeaurement = {
            type: this.selectedDrawingType,
            isDrawing: this.isDrawingMode
        };
        this.fireMeaurementEvent.next(mapMeaurement);
    }
}
