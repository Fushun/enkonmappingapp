import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { OpenLayersMapComponent } from './open-layers-map/open-layers-map.component';
import { ModalComponent } from './share/modal/modal.component';
import { TopToolbarComponent } from './top-toolbar/top-toolbar.component';
import { QueryResultComponent } from './tools/query-result/query-result.component';
import { RestDataService } from './services/rest-service';
import { DataTableModule, DropdownModule, TreeModule, AccordionModule, TabViewModule, DataListModule, SliderModule } from 'primeng/primeng';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LayerListComponent } from './tools/layer-list/layer-list.component';
import { MapComponent } from './map/map.component';
import { AppRoutingModule, routedComponents } from './app.routes';
import { AdminComponent } from './admin/admin.component';
import { ServiceListComponent } from './tools/service-list/service-list.component';
import { MapLegendComponent } from './tools/legend/legend.component';

@NgModule({
  declarations: [
    AppComponent,
    OpenLayersMapComponent,
    ModalComponent,
    TopToolbarComponent,
    QueryResultComponent,
    LayerListComponent,
    MapComponent,
    routedComponents,
    AdminComponent,
    ServiceListComponent,
    MapLegendComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    BrowserAnimationsModule,
    DataTableModule, DropdownModule, TreeModule, AccordionModule, TabViewModule, DataListModule, SliderModule,
    AppRoutingModule
  ],
  providers: [
    RestDataService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
