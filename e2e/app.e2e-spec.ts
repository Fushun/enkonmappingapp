import { MapViewerPage } from './app.po';

describe('map-viewer App', () => {
  let page: MapViewerPage;

  beforeEach(() => {
    page = new MapViewerPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
